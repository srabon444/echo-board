import HashtagList from "./Hashtag/HashtagList.tsx";
import Footer from "./Layout/Footer.tsx";
import Container from "./Layout/Container.tsx";
import { useEffect } from "react";
import { useFeedbackItemsStore } from "../stores/feedbackItemsStore.ts";

function App() {
  const fetchFeedBackItems = useFeedbackItemsStore(
    (state) => state.fetchFeedbackItems,
  );

  useEffect(() => {
    fetchFeedBackItems();
  }, [fetchFeedBackItems]);

  return (
    <div className="app">
      <Footer />

      <Container />

      <HashtagList />
    </div>
  );
}

export default App;
