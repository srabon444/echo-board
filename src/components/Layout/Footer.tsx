export default function Footer () {
    return <footer className="footer">
        <small>
            <p>&copy; {new Date().getFullYear()} Copyright by <a href="https://gitlab.com/users/srabon444/starred">Ashraful Islam</a> all rights reserved</p>

            <p><span className="u-bold u-italic">Not allowed</span> to copy without permission</p>
        </small>
    </footer>;
}
