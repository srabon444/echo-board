import Header from "./Header.tsx";
import FeedbackList from "../Feedback/FeedbackList.tsx";

export default function Container() {
  return (
    <main className="container">
      <Header />

      <FeedbackList />
    </main>
  );
}
