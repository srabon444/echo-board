import HashTagItem from "./HashTagItem.tsx";
import { useFeedbackItemsStore } from "../../stores/feedbackItemsStore.ts";

export default function HashtagList() {
  const companyList = useFeedbackItemsStore((state) => state.getCompanyList());
  const selectCompany = useFeedbackItemsStore((state) => state.selectCompany);

  return (
    <ul className="hashtags">
      {companyList.map((company) => {
        return (
          <HashTagItem
            key={company}
            company={company}
            onSelectCompany={selectCompany}
          />
        );
      })}
    </ul>
  );
}
