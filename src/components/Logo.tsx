export default function Logo() {
  return (
    <a href="/" className="logo">
      <img src="/EchoBoard.png" alt="logo" />
    </a>
  );
}
