# Echo Board

Echo Board is a simple company review application where users can review companies. The application is built using React Js, Typescript and Zustand for state management. The application is hosted on Vercel.


## [Live Application](https://echo-board.vercel.app/)

![App Screenshot 1](public/1.png)
![App Screenshot 2](public/2.png)
![App Screenshot 3](public/3.png)

## Features

- Users can add review about a company
- Users can upvote or downvote a review
- Users can filter reviews by company name hashtag

## Tech Stack
- React Js
- Typescript
- CSS
- Zustand (State Management)

**Hosting:**
- Vercel


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/srabon444/echo-board.git
```

Install dependencies

```bash
  npm install
```


